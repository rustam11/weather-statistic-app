export class WeatherData {
  id: number;
  main: string;
  description: string;
  icon: string;
}
