import {WeatherData} from './weather-data';

export class HourlyWeatherResponseData {
  dt: number;
  temp: number;
  feels_like: number;
  pressure: number;
  humidity: number;
  dew_point: number;
  clouds: number;
  wind_speed: number;
  wind_deg: number;
  weather: WeatherData[];
}
