import {CurrentWeatherResponseData} from './current-weather-response-data';
import {HourlyWeatherResponseData} from './hourly-weather-response-data';
import {DailyWeatherResponseData} from './daily-weather-response-data';

export class OpenWeatherResponseData {
  lat: number;
  lon: number;
  timezone: string;
  timezone_offset: number;
  current: CurrentWeatherResponseData;
  hourly: HourlyWeatherResponseData[];
  daily: DailyWeatherResponseData[];
}
