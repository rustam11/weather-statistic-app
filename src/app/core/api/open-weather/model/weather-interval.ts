export enum WeatherInterval {
  hourly = 'hourly',
  daily = 'daily',
}
