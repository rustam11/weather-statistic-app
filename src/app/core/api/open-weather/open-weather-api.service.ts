import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
// app imports
import {environment} from '../../../../environments/environment.prod';
import {UserPosition} from '../../../routes/home/model/user-position';
import {OpenWeatherResponseData} from './model/open-weather-response-data';

@Injectable({
  providedIn: 'root'
})
export class OpenWeatherApiService {
  private openWeatherKey = environment.openWeatherMapApiKey;

  constructor(
    private http: HttpClient,
  ) {
  }

  getForecast(userPosition: UserPosition):
    Observable<OpenWeatherResponseData> {
    const url = `http://api.openweathermap.org/data/2.5/onecall?lat=${userPosition.lat}&lon=${userPosition.lng}&appid=${this.openWeatherKey}`;
    return this.http.get(url) as Observable<OpenWeatherResponseData>;
  }
}
