import {Injectable} from '@angular/core';
import * as moment from 'moment';
// app imports
import {WeatherInterval} from '../api/open-weather/model/weather-interval';

@Injectable({
  providedIn: 'root'
})

export class CommonUtilitiesService {

  constructor() {
  }

  transformUnixToLocalDatetime(unixDatetime: number, weatherInterval: WeatherInterval, momentLocale: string = 'en'): string {
    const momentInput: moment.Moment = moment.unix(unixDatetime);
    switch (weatherInterval) {
      case WeatherInterval.daily:
        return moment(momentInput, 'YYYY-MM-DD').locale(momentLocale).format('MMMM D, YYYY');
      case WeatherInterval.hourly:
        return moment(momentInput, 'YYYY-MM-DD H').locale(momentLocale).format('hha ll');
      default:
        return unixDatetime.toString();
    }
  }
}
