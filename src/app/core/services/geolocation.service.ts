import {Injectable} from '@angular/core';
// app imports
import {UserPosition} from '../../routes/home/model/user-position';

@Injectable({
  providedIn: 'root'
})
export class GeolocationService {

  constructor() {
  }

  getUserCurrentGeolocation(): Promise<UserPosition> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition((position: Position) => {
          resolve({lng: position.coords.longitude, lat: position.coords.latitude});
        },
        err => {
          reject(err);
        });
    });
  }
}
