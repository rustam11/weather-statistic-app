import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {OnDestroyMixin, untilComponentDestroyed} from '@w11k/ngx-componentdestroyed';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import * as _ from 'lodash';
// app imports
import {LoaderService} from '../../../../shared/loader/services/loader.service';
import {OpenWeatherResponseData} from '../../../../core/api/open-weather/model/open-weather-response-data';
import {WidgetItem} from '../../model/widget-item';
import {SelectOptionGroupFactoryService} from '../../services/select-option-group-factory.service';
import {SelectOptionGroup} from '../../model/select-option-group';
import {WeatherInterval} from '../../../../core/api/open-weather/model/weather-interval';
import {ConfirmationDialogComponent} from '../../../../shared/dialog/components/confirmation-dialog/confirmation-dialog.component';
import {DialogComponentData} from '../../../../shared/dialog/model/dialog-component-data';
import {AddNewSensorDialogComponent} from '../../../../shared/dialog/components/add-new-sensor-dialog/add-new-sensor-dialog.component';
import {ChartConfig} from '../../model/chart-config';
import {WidgetListService} from '../../services/widget-list.service';
import {LocalStorageService} from '../../../../shared/storage/local-storage.service';
import {widgetListKey} from '../../model/constants';

@Component({
  selector: 'app-widgets-wrapper',
  templateUrl: './widgets-wrapper.component.html',
  styleUrls: ['./widgets-wrapper.component.scss']
})
export class WidgetsWrapperComponent extends OnDestroyMixin implements OnInit, OnDestroy {
  @Input() set dataSource(dataSource: OpenWeatherResponseData) {
    if (dataSource) {
      this._dataSource = dataSource;
      this.userCityName = this.getUserCityName(dataSource.timezone);
    }
  }

  _dataSource: OpenWeatherResponseData;
  widgetList: WidgetItem[];
  userCityName: string;
  isLoading: boolean;

  weatherForecastMode: SelectOptionGroup[];
  weatherInterval: WeatherInterval = WeatherInterval.daily;

  private confirmationDialogRef: MatDialogRef<ConfirmationDialogComponent, boolean>;
  private addNewChartDialogRef: MatDialogRef<AddNewSensorDialogComponent, ChartConfig>;

  constructor(
    private loaderService: LoaderService,
    private selectOptionGroupFactoryService: SelectOptionGroupFactoryService,
    private dialog: MatDialog,
    private widgetListService: WidgetListService,
    private localStorageService: LocalStorageService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.initWidgetListSubscription();
    this.initSelectOptions();
    this.handleLoadingState();
  }

  private initWidgetListSubscription(): void {
    this.widgetListService.asObservable()
      .pipe(untilComponentDestroyed(this))
      .subscribe((widgetList: WidgetItem[]) => {
        this.widgetList = widgetList;
      });
  }

  private getUserCityName(timezone: string): string {
    return timezone.substring(timezone.lastIndexOf('/') + 1);
  }

  private initSelectOptions(): void {
    this.weatherForecastMode = this.selectOptionGroupFactoryService.setWeatherForecastMode();
  }

  trackByFn(index: number, config: WidgetItem): number | string {
    return config.chartConfig.length;
  }

  private handleLoadingState(): void {
    this.loaderService.asObservable()
      .pipe(untilComponentDestroyed(this))
      .subscribe((isLoading: boolean) => {
        this.isLoading = isLoading;
      });
  }

  onForecastModeChange(event: string): void {
    this.weatherInterval = event as WeatherInterval;
  }

  onAddChart(widget: WidgetItem, index): void {
    const sensorsList: SelectOptionGroup[] = this.selectOptionGroupFactoryService.setSensors().filter((sensor: SelectOptionGroup) =>
      !_.isEqual(sensor, widget.chartConfig[0].sensor)
    );
    const lineColorList: SelectOptionGroup[] = this.selectOptionGroupFactoryService.setLineColorList().filter((color: SelectOptionGroup) =>
      color.value !== widget.chartConfig[0].color
    );
    this.addNewChartDialogRef = this.dialog.open(AddNewSensorDialogComponent, {
      width: '450px',
      data: {
        title: 'Add new Sensor',
        sensorsList,
        lineColorList,
      } as DialogComponentData,
    });
    this.addNewChartDialogRef.afterClosed()
      .subscribe((chartConfig: ChartConfig) => {
        if (chartConfig) {
          this.widgetList[index].chartConfig.push(chartConfig);
          this.widgetListService.set(this.widgetList);
          this.localStorageService.setItem(widgetListKey, this.widgetList);
        }
      });
  }

  onRemoveChart(index: number): void {
    this.confirmationDialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '450px',
      data: {
        title: 'Are you sure',
        content: 'You want to delete this widget?',
      } as DialogComponentData,
    });
    this.confirmationDialogRef.afterClosed()
      .subscribe((removeChart: boolean) => {
        if (removeChart) {
          this.widgetList.splice(index, 1);
          this.widgetListService.set(this.widgetList);
          this.localStorageService.setItem(widgetListKey, this.widgetList);
          // we need to trigger resize if grid container increased when we have only one widget
          // to update chart width
          if (this.widgetList.length === 1) {
            window.dispatchEvent(new Event('resize'));
          }
        }
      });
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    if (this.confirmationDialogRef) {
      this.confirmationDialogRef.close();
    }
    if (this.addNewChartDialogRef) {
      this.addNewChartDialogRef.close();
    }
  }
}
