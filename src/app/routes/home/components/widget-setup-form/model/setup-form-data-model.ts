import {SelectOptionGroup} from '../../../model/select-option-group';
import {ChartColors} from '../../../model/chart-colors';

export class SetupFormDataModel {
  type: string;
  sensor: SelectOptionGroup;
  color: ChartColors;
}
