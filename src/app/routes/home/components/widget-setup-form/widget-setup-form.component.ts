import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
// app imports
import {SelectOptionGroup} from '../../model/select-option-group';
import {SelectOptionGroupFactoryService} from '../../services/select-option-group-factory.service';
import {SetupFormDataModel} from './model/setup-form-data-model';

@Component({
  selector: 'app-widget-setup-form',
  templateUrl: './widget-setup-form.component.html',
  styleUrls: ['./widget-setup-form.component.scss']
})
export class WidgetSetupFormComponent implements OnInit {
  @ViewChild('formDirective', {static: false}) formDirective: FormGroupDirective;
  @Output() formSubmitted: EventEmitter<SetupFormDataModel> = new EventEmitter<SetupFormDataModel>();
  @Input() errorMessage: string;
  widgetConfigForm: FormGroup;

  chartTypes: SelectOptionGroup[];
  sensorsList: SelectOptionGroup[];
  lineColorList: SelectOptionGroup[];

  constructor(
    private formBuilder: FormBuilder,
    private selectOptionGroupFactoryService: SelectOptionGroupFactoryService,
  ) {
  }

  ngOnInit(): void {
    this.initSelectOptions();
    this.initForm();
  }

  private initSelectOptions(): void {
    this.chartTypes = this.selectOptionGroupFactoryService.setChartType();
    this.sensorsList = this.selectOptionGroupFactoryService.setSensors();
    this.lineColorList = this.selectOptionGroupFactoryService.setLineColorList();
  }

  private initForm(): void {
    this.widgetConfigForm = this.formBuilder.group({
      type: ['', [
        Validators.required,
      ]],
      sensor: ['', [
        Validators.required,
      ]],
      color: ['', [
        Validators.required,
      ]],
    });
  }

  resetForm(): void {
    this.formDirective.resetForm();
    this.widgetConfigForm.reset();
  }

}
