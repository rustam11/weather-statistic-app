import {Component, OnInit, ViewChild} from '@angular/core';
import {OnDestroyMixin, untilComponentDestroyed} from '@w11k/ngx-componentdestroyed';
import {take} from 'rxjs/operators';
import * as _ from 'lodash';
// app imports
import {GeolocationService} from '../../../../core/services/geolocation.service';
import {UserPosition} from '../../model/user-position';
import {OpenWeatherApiService} from '../../../../core/api/open-weather/open-weather-api.service';
import {WidgetItem} from '../../model/widget-item';
import {SnackBarService} from '../../../../shared/snack-bar/snack-bar.service';
import {OpenWeatherResponseData} from '../../../../core/api/open-weather/model/open-weather-response-data';
import {WidgetSetupFormComponent} from '../widget-setup-form/widget-setup-form.component';
import {SetupFormDataModel} from '../widget-setup-form/model/setup-form-data-model';
import {WidgetListService} from '../../services/widget-list.service';
import {LocalStorageService} from '../../../../shared/storage/local-storage.service';
import {widgetListKey} from '../../model/constants';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends OnDestroyMixin implements OnInit {
  @ViewChild(WidgetSetupFormComponent, {static: true})
  widgetSetupForm: WidgetSetupFormComponent;
  private userPosition: UserPosition;
  dataSource: OpenWeatherResponseData = null;
  errorMessage: string;

  constructor(
    private geolocationService: GeolocationService,
    private openWeatherApiService: OpenWeatherApiService,
    private snackBarService: SnackBarService,
    private widgetListService: WidgetListService,
    private localStorageService: LocalStorageService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.getUserCurrentGeolocation(() => {
      this.getStoredWidgetList();
    });
  }


  private getUserCurrentGeolocation(complete = () => {
  }): void {
    this.geolocationService.getUserCurrentGeolocation().then((position: UserPosition) => {
      this.userPosition = position;
      this.errorMessage = null;
      complete();
    }).catch(() => {
      this.userDidntGivePermission();
      complete();
    });
  }

  private getStoredWidgetList(): void {
    const storedWidgetList: WidgetItem[] = this.localStorageService.getItem(widgetListKey);
    if (storedWidgetList && storedWidgetList.length) {
      this.widgetListService.set(storedWidgetList);
      this.fetch();
    }
  }

  fetchData(widgetData: SetupFormDataModel): void {
    const widgetToAdd: WidgetItem = {
      type: widgetData.type,
      chartConfig: [{
        sensor: widgetData.sensor,
        color: widgetData.color
      }]
    };
    const widgetItems: WidgetItem[] = this.widgetListService.get();
    if (_.some(widgetItems, widgetToAdd)) {
      this.snackBarService.showSnackbar('Widget already presented on a dashboard', null, 3000);
    } else {
      if (widgetItems.length < 4) {
        this.fetchAndBuildWidget(widgetToAdd);
        this.widgetSetupForm.resetForm();
      } else {
        this.snackBarService.showSnackbar('You have reached maximum number of dashboard widgets', null, 3000);
      }
    }
  }

  private fetchAndBuildWidget(widget: WidgetItem): void {
    this.constructAndStoreWidgetList(widget);
    this.fetch();
  }

  private constructAndStoreWidgetList(widget: WidgetItem): void {
    const widgetList: WidgetItem[] = this.widgetListService.get();
    widgetList.push(widget);
    this.widgetListService.set(widgetList);
    this.localStorageService.setItem(widgetListKey, widgetList);
  }

  private fetch(): void {
    if (this.userPosition) {
      this.openWeatherApiService.getForecast(this.userPosition)
        .pipe(
          take(1),
          untilComponentDestroyed(this),
        )
        .subscribe((dataSource: OpenWeatherResponseData) => {
          this.errorMessage = null;
          this.dataSource = dataSource;
        }, error => {
          this.widgetListService.set([]);
          this.localStorageService.setItem(widgetListKey, []);
          this.errorMessage = error.message;
        });
    } else {
      this.userDidntGivePermission();
    }
  }

  private userDidntGivePermission(): void {
    const userMessage = 'In order to work with the application, please allow us to use of your geo position data';
    this.errorMessage = userMessage;
    this.snackBarService.showSnackbar(userMessage, null, 3000);
  }
}
