import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatMenuModule} from '@angular/material/menu';
import {FlexLayoutModule} from '@angular/flex-layout';
// app imports
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {HomeRoutingModule} from './home-routing.module';
import {HighChartsModule} from '../../shared/charts/high-charts.module';
import {WidgetSetupFormComponent} from './components/widget-setup-form/widget-setup-form.component';
import {LoaderModule} from '../../shared/loader/loader.module';
import {WidgetsWrapperComponent} from './components/widgets-wrapper/widgets-wrapper.component';
import {ToggleGroupModule} from '../../shared/toggle-group/toggle-group.module';
import {DialogModule} from '../../shared/dialog/dialog.module';

@NgModule({
  declarations: [
    DashboardComponent,
    WidgetSetupFormComponent,
    WidgetsWrapperComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HomeRoutingModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    FlexLayoutModule,
    HighChartsModule,
    LoaderModule,
    ToggleGroupModule,
    MatIconModule,
    DialogModule,
    MatMenuModule,
  ],
})

export class HomeModule {
}
