import {Injectable} from '@angular/core';
// app imports
import {SelectOptionGroup} from '../model/select-option-group';
import {ChartColors} from '../model/chart-colors';
import {WeatherInterval} from '../../../core/api/open-weather/model/weather-interval';
import {Sensor} from '../model/sensor';

@Injectable({
  providedIn: 'root'
})
export class SelectOptionGroupFactoryService {

  constructor() {
  }

  setChartType(): SelectOptionGroup[] {
    return [
      {value: 'spline', viewValue: 'Line'},
      {value: 'column', viewValue: 'Column'},
    ];
  }

  setSensors(): SelectOptionGroup[] {
    return [
      {value: Sensor.humidity, viewValue: 'Humidity'},
      {value: Sensor.pressure, viewValue: 'Pressure'},
      {value: Sensor.temp, viewValue: 'Temperature'},
      {value: Sensor.wind_speed, viewValue: 'Wind Speed'},
    ];
  }

  setLineColorList(): SelectOptionGroup[] {
    return [
      {value: ChartColors.blue, viewValue: 'Blue'},
      {value: ChartColors.darkOrange, viewValue: 'Dark Orange'},
      {value: ChartColors.gray, viewValue: 'Grey'},
      {value: ChartColors.green, viewValue: 'Green'},
      {value: ChartColors.orange, viewValue: 'Orange'},
      {value: ChartColors.red, viewValue: 'Red'},
    ];
  }

  setWeatherForecastMode(): SelectOptionGroup[] {
    return [
      {value: WeatherInterval.hourly, viewValue: 'Forecast for 48 hours'},
      {value: WeatherInterval.daily, viewValue: 'Forecast for 7 days'},
    ];
  }
}
