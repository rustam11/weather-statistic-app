import {Injectable} from '@angular/core';
// app imports
import {PubSub} from '../../../shared/pub-sub/pub-sub';
import {WidgetItem} from '../model/widget-item';

@Injectable({
  providedIn: 'root'
})
export class WidgetListService extends PubSub<WidgetItem[]>{

  constructor() {
    super([]);
  }

}
