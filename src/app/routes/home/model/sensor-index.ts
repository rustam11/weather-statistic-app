export enum SensorIndex {
  temp = 'C',
  pressure = 'hPa',
  humidity = '%',
  wind_speed = 'm/s',
}
