export enum ChartColors {
  orange = '#F59923',
  darkOrange = '#DD6815',
  red = '#E24A48',
  blue = '#31A3D1',
  green = '#45B599',
  gray = '#3F3F3F',
}
