import {ChartConfig} from './chart-config';

export class WidgetItem {
  type: string;
  chartConfig: ChartConfig[];
}
