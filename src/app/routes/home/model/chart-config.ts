import {SelectOptionGroup} from './select-option-group';

export class ChartConfig {
  sensor: SelectOptionGroup;
  color: string;
}
