export enum Sensor {
  humidity = 'humidity',
  pressure = 'pressure',
  temp = 'temp',
  wind_speed = 'wind_speed',
}
