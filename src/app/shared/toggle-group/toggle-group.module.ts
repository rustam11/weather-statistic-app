import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
// app imports
import {ToggleGroupComponent} from './components/toggle-group.component';

@NgModule({
  declarations: [
    ToggleGroupComponent,
  ],
  imports: [
    CommonModule,
    MatButtonToggleModule,
  ],
  exports: [
    ToggleGroupComponent,
  ]
})

export class ToggleGroupModule {
}
