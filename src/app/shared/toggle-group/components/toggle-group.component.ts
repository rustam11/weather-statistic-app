import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
// app imports
import {SelectOptionGroup} from '../../../routes/home/model/select-option-group';


@Component({
  selector: 'app-toggle-group',
  templateUrl: './toggle-group.component.html',
  styleUrls: ['./toggle-group.component.scss']
})
export class ToggleGroupComponent implements OnInit {
  @Input() value: string;
  @Input() options: SelectOptionGroup[];
  @Output() optionChanged: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit(): void {
  }
}
