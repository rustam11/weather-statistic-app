import {Injectable} from '@angular/core';
// app imports

@Injectable({
  providedIn: 'root'
})

export class LocalStorageService {
  constructor() {
  }

  setItem<T>(key: string, data: T): void {
    localStorage.setItem(key, JSON.stringify(data));
  }

  getItem<T>(key: string): T {
    return JSON.parse(localStorage.getItem(key));
  }

}
