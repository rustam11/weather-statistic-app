import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
// app imports

@Injectable({
  providedIn: 'root'
})

export class SnackBarService {

  constructor(private snackBar: MatSnackBar) {
  }

  showSnackbar(message, action, duration): void {
    this.snackBar.open(message, action, {
      duration,
    });
  }
}
