import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
// app imports
import {SelectOptionComponent} from './component/select-option.component';

@NgModule({
  declarations: [
    SelectOptionComponent,
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatSelectModule,
  ],
  exports: [
    SelectOptionComponent,
  ]
})

export class SelectOptionModule {

}
