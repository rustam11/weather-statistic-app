import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
// app imports
import {SelectOptionGroup} from '../../../routes/home/model/select-option-group';

@Component({
  selector: 'app-select-option',
  templateUrl: './select-option.component.html',
  styleUrls: ['./select-option.component.scss']
})
export class SelectOptionComponent implements OnInit {
  @Input() label: string;
  @Input() options: SelectOptionGroup[];

  @Input() set selectedValue(value: string) {
    if (value) {
      this._selectedValue = value;
    }
  }

  @Output() selectionChanged: EventEmitter<string> = new EventEmitter<string>();
  _selectedValue: string;

  constructor() {
  }

  ngOnInit(): void {
  }
}
