import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HighchartsChartModule} from 'highcharts-angular';
// app imports
import {HighChartsComponent} from './components/high-charts/high-charts.component';
import {ChartConfigEditorComponent} from './components/chart-config-editor/chart-config-editor.component';
import {SelectOptionModule} from '../select-option/select-option.module';

@NgModule({
  declarations: [
    HighChartsComponent,
    ChartConfigEditorComponent,
  ],
  imports: [
    CommonModule,
    HighchartsChartModule,
    SelectOptionModule,
    FlexLayoutModule,
  ],
  exports: [
    HighChartsComponent,
    ChartConfigEditorComponent,
  ]
})

export class HighChartsModule {

}
