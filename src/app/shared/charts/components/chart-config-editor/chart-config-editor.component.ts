import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
// app imports
import {WidgetItem} from '../../../../routes/home/model/widget-item';
import {SelectOptionGroup} from '../../../../routes/home/model/select-option-group';
import {SelectOptionGroupFactoryService} from '../../../../routes/home/services/select-option-group-factory.service';
import {ChartConfig} from '../../../../routes/home/model/chart-config';

@Component({
  selector: 'app-chart-config-editor',
  templateUrl: './chart-config-editor.component.html',
  styleUrls: ['./chart-config-editor.component.scss']
})
export class ChartConfigEditorComponent implements OnInit {
  @Input() set widget(config: WidgetItem) {
    if (config) {
      this._widget = config;
    }
  }

  @Output() widgetConfigChanged: EventEmitter<WidgetItem> = new EventEmitter<WidgetItem>();

  lineColorList: SelectOptionGroup[];
  chartTypes: SelectOptionGroup[];
  _widget: WidgetItem;


  constructor(
    private selectOptionGroupFactoryService: SelectOptionGroupFactoryService,
  ) {
  }

  ngOnInit(): void {
    this.initSelectOptions();
  }

  private initSelectOptions(): void {
    this.lineColorList = this.selectOptionGroupFactoryService.setLineColorList();
    this.chartTypes = this.selectOptionGroupFactoryService.setChartType();
  }

  onLineColorChange(color: string, index: number): void {
    const updatedConfig: ChartConfig = {...this._widget.chartConfig[index], color};
    this._widget.chartConfig[index] = updatedConfig;
    this.widgetConfigChanged.emit(this._widget);
  }

  onChartTypeChange(type: string): void {
    const updatedWidgetItem: WidgetItem = {...this._widget, type};
    this.widgetConfigChanged.emit(updatedWidgetItem);
  }
}
