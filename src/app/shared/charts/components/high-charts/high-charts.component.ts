import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import * as Highcharts from 'highcharts';
// app imports
import {OpenWeatherResponseData} from '../../../../core/api/open-weather/model/open-weather-response-data';
import {WidgetItem} from '../../../../routes/home/model/widget-item';
import {DailyWeatherResponseData} from '../../../../core/api/open-weather/model/daily-weather-response-data';
import {HourlyWeatherResponseData} from '../../../../core/api/open-weather/model/hourly-weather-response-data';
import {CommonUtilitiesService} from '../../../../core/services/common-utilities.service';
import {WeatherInterval} from '../../../../core/api/open-weather/model/weather-interval';
import {ChartConfig} from '../../../../routes/home/model/chart-config';
import {WidgetListService} from '../../../../routes/home/services/widget-list.service';
import {widgetListKey} from '../../../../routes/home/model/constants';
import {LocalStorageService} from '../../../storage/local-storage.service';
import {Sensor} from '../../../../routes/home/model/sensor';
import {SensorIndex} from '../../../../routes/home/model/sensor-index';

@Component({
  selector: 'app-high-charts',
  templateUrl: './high-charts.component.html',
  styleUrls: ['./high-charts.component.scss']
})
export class HighChartsComponent implements OnInit, OnChanges {
  @Input() dataSource: OpenWeatherResponseData;
  @Input() weatherInterval: WeatherInterval;
  @Input() widgetIndex: number;

  @Input() set widget(config: WidgetItem) {
    if (config) {
      this._widget = config;
    }
  }

  highCharts: typeof Highcharts = Highcharts;
  _widget: WidgetItem;
  private seriesData: Highcharts.SeriesOptionsType[];
  chartOptions: Highcharts.Options;

  constructor(
    private commonUtilitiesService: CommonUtilitiesService,
    private widgetListService: WidgetListService,
    private localStorageService: LocalStorageService,
  ) {
  }

  ngOnInit(): void {
    this.drawChart(this.dataSource);
  }

  ngOnChanges(changes: SimpleChanges): void {

    if (changes.weatherInterval && !changes.weatherInterval.firstChange) {
      this.drawChart(this.dataSource);
    }

    if (changes.widget && !changes.widget.firstChange) {
      this.drawChart(this.dataSource);
    }
  }


  private drawChart(weatherResponseData: OpenWeatherResponseData): void {

    if (weatherResponseData) {
      this.seriesData = this.generateSeriesData(weatherResponseData, this._widget);

      this.chartOptions = {
        chart: {
          type: this._widget.type,
          style: {
            fontFamily: 'Roboto, sans-serif',
            fontSize: '13px',
          },
          height: 320,
        },
        credits: {
          enabled: false
        },
        title: {
          text: '',
        },
        xAxis: this.generateXAxisOptions(weatherResponseData),
        yAxis: this.generateYAxisOptions(),
        tooltip: this.generateTooltipOptions(),
        series: this.seriesData,
      };
    }
  }

  private generateSeriesData(weatherResponseData: OpenWeatherResponseData, widgetConfig: WidgetItem): Highcharts.SeriesOptionsType[] {

    const chartSchema: (Highcharts.SeriesSplineOptions | Highcharts.SeriesColumnOptions)[] = [];
    widgetConfig.chartConfig.forEach((configItem: ChartConfig) => {
        const histogram: Highcharts.SeriesSplineOptions | Highcharts.SeriesColumnOptions =
          this.generateHistogram(weatherResponseData, configItem);
        chartSchema.push(histogram);
      }
    );
    return chartSchema;
  }

  private generateHistogram(weatherResponseData, chartConfig: ChartConfig):
    Highcharts.SeriesSplineOptions | Highcharts.SeriesColumnOptions {
    const sensorIndex: SensorIndex = this.generateSeriesNameIndex(chartConfig);

    return {
      data: this.generateChartAxisDataForGivenSensor(
        weatherResponseData,
        chartConfig.sensor.value as Sensor,
        this.weatherInterval
      ),
      name: chartConfig.sensor.viewValue + ', ' + sensorIndex,
      color: chartConfig.color,
      marker: {
        enabled: false
      },
      connectNulls: true,
    } as Highcharts.SeriesSplineOptions | Highcharts.SeriesColumnOptions;
  }

  private generateSeriesNameIndex(chartConfig: ChartConfig): SensorIndex {
    switch (chartConfig.sensor.value) {
      case Sensor.humidity:
        return SensorIndex.humidity;
      case Sensor.pressure:
        return SensorIndex.pressure;
      case Sensor.temp:
        return SensorIndex.temp;
      case Sensor.wind_speed:
        return SensorIndex.wind_speed;
      default:
        return '' as SensorIndex;
    }
  }

  private generateXAxisOptions(weatherResponseData: OpenWeatherResponseData): Highcharts.XAxisOptions {
    return {
      categories: this.generateCategoriesLabels(weatherResponseData, this.weatherInterval),
      labels: {
        style: {
          fontSize: '12px',
          whiteSpace: 'normal',
          textAlign: 'center',
        },
        autoRotation: [0, -90],
        useHTML: true,
        formatter(): string {
          return this.value.toString().replace('\n', '<br/>');
        }
      }
    };
  }

  private generateCategoriesLabels(weatherResponseData: OpenWeatherResponseData, weatherInterval: WeatherInterval): string[] {
    if (!weatherResponseData) {
      return [];
    }

    return weatherResponseData[weatherInterval.toString()].map((responseData: DailyWeatherResponseData | HourlyWeatherResponseData) =>
      this.commonUtilitiesService.transformUnixToLocalDatetime(responseData.dt, weatherInterval)
    );
  }

  private generateYAxisOptions(): Highcharts.YAxisOptions {
    return {
      title: {
        text: '',
        style: {
          fontSize: '13px',
        }
      },
      lineWidth: 2,
      gridLineWidth: .5,
      labels: {
        style: {
          fontSize: '12px',
        },
      },
    };
  }

  private generateTooltipOptions(): Highcharts.TooltipOptions {
    return {
      shared: true,
      useHTML: true,
      headerFormat: '<span style="font-size:13px; font-weight: 500; margin-bottom: 5px; display: block">{point.x:%b-%Y}</span>',
      pointFormatter(): string {
        const seriesName: string = this.series.name.substring(0, this.series.name.indexOf(','));
        return `
                <span style="color: ${this.color}">●</span>
                <span style="font-size: 14px">${seriesName}</span>
                <span style="font-weight: 700; font-size: 14px">${this.y}</span><br/>
               `;
      },
    };
  }

  private generateChartAxisDataForGivenSensor<DatePeriod>(
    weatherResponseData: OpenWeatherResponseData,
    sensor: Sensor,
    weatherInterval: WeatherInterval,
  ): number[] {

    if (weatherResponseData) {
      switch (sensor) {
        case Sensor.temp:
          switch (weatherInterval) {
            case WeatherInterval.daily:
              const maxTempPath = 'max';
              const minTempPath = 'min';
              return weatherResponseData[weatherInterval.toString()].map((datePeriod: DatePeriod) =>
                this.calculateAvgTemp(datePeriod, sensor, maxTempPath, minTempPath)
              );
            default:
              return weatherResponseData[weatherInterval.toString()].map((datePeriod: DatePeriod) =>
                this.transformFahrenheitToCelsius(datePeriod[sensor]));
          }
        default:
          return weatherResponseData[weatherInterval.toString()].map((datePeriod: DatePeriod) => datePeriod[sensor]);
      }
    } else {
      return null;
    }
  }

  private calculateAvgTemp<DatePeriod>(datePeriod: DatePeriod, sensor: string, maxTempPath: string, minTempPath: string): number {
    const maxTemp = datePeriod[sensor][maxTempPath];
    const minTemp = datePeriod[sensor][minTempPath];
    const avgTemp = (maxTemp + minTemp) / 2;
    return this.transformFahrenheitToCelsius(avgTemp);
  }


  private transformFahrenheitToCelsius(tempInFahrenheit: number): number {
    return Math.floor(tempInFahrenheit - 273.15);
  }

  onWidgetConfigChange(config: WidgetItem): void {
    const widgetList: WidgetItem[] = this.widgetListService.get();
    widgetList[this.widgetIndex] = config;
    this.widgetListService.set(widgetList);
    this.localStorageService.setItem(widgetListKey, widgetList);
    this.drawChart(this.dataSource);
  }
}
