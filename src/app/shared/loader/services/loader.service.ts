import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable()
export class LoaderService {
  private _isLoading = new Subject<boolean>();

  constructor() {
  }

  show(): void {
    this._isLoading.next(true);
  }

  hide(): void {
    this._isLoading.next(false);
  }

  asObservable(): Observable<boolean> {
    return this._isLoading.asObservable();
  }
}
