import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
// app imports
import {DialogComponentData} from '../../model/dialog-component-data';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean>,
    @Inject(MAT_DIALOG_DATA) public data: DialogComponentData,
  ) {
  }

  onClose(): void {
    this.dialogRef.close();
  }

  closeWithOk(): void {
    this.dialogRef.close(true);
  }
}
