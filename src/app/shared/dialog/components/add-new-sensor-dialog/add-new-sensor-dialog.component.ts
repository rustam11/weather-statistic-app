import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
// app imports
import {AddNewChartDialogData} from '../../model/add-new-chart-dialog-data';
import {ChartConfig} from '../../../../routes/home/model/chart-config';
import {SelectOptionGroup} from '../../../../routes/home/model/select-option-group';

@Component({
  selector: 'app-add-new-sensor-dialog',
  templateUrl: './add-new-sensor-dialog.component.html',
  styleUrls: ['./add-new-sensor-dialog.component.scss']
})
export class AddNewSensorDialogComponent {

  chartConfig: ChartConfig;
  disabled = true;

  constructor(
    public dialogRef: MatDialogRef<AddNewSensorDialogComponent, ChartConfig>,
    @Inject(MAT_DIALOG_DATA) public data: AddNewChartDialogData,
  ) {
  }

  onClose(): void {
    this.dialogRef.close();
  }

  addNewChart(): void {
    this.dialogRef.close(this.chartConfig);
  }

  onLineColorChange(color: string): void {
    this.chartConfig = {
      ...this.chartConfig,
      color,
    };
    if (this.checkDisabled(this.chartConfig)) {
      this.disabled = false;
    }
  }

  onChartSensorChange(event: string): void {
    const [selectedSensor] = this.data.sensorsList.filter((sensor: SelectOptionGroup) =>
      sensor.value === event);
    this.chartConfig = {
      ...this.chartConfig,
      sensor: selectedSensor,
    };
    if (this.checkDisabled(this.chartConfig)) {
      this.disabled = false;
    }
  }

  private checkDisabled(chartConfig: ChartConfig): boolean {
    return chartConfig.hasOwnProperty('color') && chartConfig.hasOwnProperty('sensor');
  }
}
