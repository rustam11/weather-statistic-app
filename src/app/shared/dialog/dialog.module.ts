import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {FlexLayoutModule} from '@angular/flex-layout';
// app imports
import {ConfirmationDialogComponent} from './components/confirmation-dialog/confirmation-dialog.component';
import {AddNewSensorDialogComponent} from './components/add-new-sensor-dialog/add-new-sensor-dialog.component';
import {SelectOptionModule} from '../select-option/select-option.module';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    FlexLayoutModule,
    SelectOptionModule,
  ],
  declarations: [
    ConfirmationDialogComponent,
    AddNewSensorDialogComponent
  ],
  entryComponents: [
    ConfirmationDialogComponent,
    AddNewSensorDialogComponent,
  ],
})
export class DialogModule {

}
