import {DialogComponentData} from './dialog-component-data';
import {SelectOptionGroup} from '../../../routes/home/model/select-option-group';

export class AddNewChartDialogData extends DialogComponentData {
  sensorsList: SelectOptionGroup[];
  lineColorList: SelectOptionGroup[];
}
