export class DialogComponentData {
  title: string;
  content?: string;
}
